.. _mod-version:

Version
-------

Module checks for new version and CVEs_.

Running
^^^^^^^

.. code-block:: lua

	   modules.load("version")

.. _cves: https://cve.mitre.org/
